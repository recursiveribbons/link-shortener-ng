import { Injectable } from '@angular/core';
import {Link} from './link';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LinkApiService {

  private apiUrl = 'http://robintest.cf/links';

  constructor(private http: HttpClient) { }

  getLinks(): Observable<Link[]> {
    return this.http.get<Link[]>(this.apiUrl);
  }

  newLink(link: Link): Observable<Link> {
    return this.http.post<Link>(this.apiUrl, link, httpOptions);
  }

  updateLink(link: Link): Observable<any> {
    const url = `${this.apiUrl}/${link.linkId}`;
    return this.http.put(url, link, httpOptions);
  }

  deleteLink(linkId: string): Observable<any> {
    const url = `${this.apiUrl}/${linkId}`;
    return this.http.delete(url);
  }
}
