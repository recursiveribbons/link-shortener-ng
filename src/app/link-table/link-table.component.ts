import { Component, OnInit } from '@angular/core';
import { Link } from '../link';
import { LinkApiService } from '../link-api.service';

@Component({
  selector: 'app-link-table',
  templateUrl: './link-table.component.html',
  styleUrls: ['./link-table.component.css']
})
export class LinkTableComponent implements OnInit {
  links: Link[];
  newLink: Link = {linkId: '', linkURL: ''};
  constructor(private linkApiService: LinkApiService) { }

  ngOnInit() {
    this.getLinks();
  }

  getLinks(): void {
    this.linkApiService.getLinks()
      .subscribe(
        (links: Link[]) => this.links = links,
        () => alert('Error communicating with server'));
  }

  change(linkId: string) {
    const i = this.findLinkId(linkId);
    this.linkApiService.updateLink(this.links[i])
      .subscribe(
        (link: Link) => this.links[i] = link,
        () => alert('Error communicating with server')
      );
  }

  delete(linkId: string) {
    this.linkApiService.deleteLink(linkId)
      .subscribe(
        () => {
          const i = this.findLinkId(linkId);
          this.links.splice(i, 1);
        },
        () => alert('Error communicating with server')
      );
  }

  new() {
    this.linkApiService.newLink(this.newLink)
      .subscribe(
        (link: Link) => {
          this.links.push(link);
          this.newLink = {linkId: '', linkURL: ''};
          },
        () => alert('Error communicating with server'));
  }

  private findLinkId(linkId: string): number {
    return this.links.findIndex(link => link.linkId === linkId);
  }
}
