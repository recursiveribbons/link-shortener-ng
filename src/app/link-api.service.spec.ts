import { TestBed, inject } from '@angular/core/testing';

import { LinkApiService } from './link-api.service';

describe('LinkApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LinkApiService]
    });
  });

  it('should be created', inject([LinkApiService], (service: LinkApiService) => {
    expect(service).toBeTruthy();
  }));
});
