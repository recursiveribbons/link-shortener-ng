FROM node:8.11.3-alpine as builder
RUN npm i npm@latest -g
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run ng build --prod --build-optimizer

FROM nginx:1.15
COPY --from=builder /app/dist/link-shortener-ng/ /usr/share/nginx/html
COPY /nginx.conf /etc/nginx/conf.d/default.conf
